import { StyleSheet, Text, View } from 'react-native';

import ProductContainer from './Screens/Products/ProductContainer.js';
import Header from './Shared/Header';

export default function App() {
  return (
    <View style={styles.container}>
      <Header />

      <ProductContainer />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
